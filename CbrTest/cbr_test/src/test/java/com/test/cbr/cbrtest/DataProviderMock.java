/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest;

import com.test.cbr.cbrtest.data.dataholder.DataHolder;
import com.test.cbr.cbrtest.data.dataholder.IDataHolder;
import com.test.cbr.cbrtest.data.repository.IDataProvider;
import com.test.cbr.cbrtest.domain.events.IEventBus;
import com.test.cbr.cbrtest.domain.model.CurrencyTag;

import java.util.ArrayList;

/**
 * Created by admin on 23.02.2017.
 */

public class DataProviderMock extends Thread implements IDataProvider {

    final private static String TAG = "DataProviderMock";

    private IEventBus eventBus;

    public DataProviderMock(IDataHolder dataHolder, IEventBus eventBus) {
        this.dataHolder = dataHolder;
        this.eventBus = eventBus;
    }


    @Override
    public void loadData() {
        eventBus.publish(IEventBus.CURRENCIES_PROVIDED, dataHolder);
    }

    @Override
    public void updateData(IDataHolder dataHolder) {
        eventBus.publish(IEventBus.CURRENCIES_UPDATED_LOCAL, dataHolder);
    }

    private IDataHolder dataHolder = new DataHolder(new ArrayList<CurrencyTag>());

}
