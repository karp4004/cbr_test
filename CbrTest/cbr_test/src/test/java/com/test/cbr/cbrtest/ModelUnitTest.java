/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest;

import com.test.cbr.cbrtest.data.dataholder.DataHolder;
import com.test.cbr.cbrtest.data.dataholder.IDataHolder;
import com.test.cbr.cbrtest.data.repository.IDataProvider;
import com.test.cbr.cbrtest.domain.events.IEventSubscriber;
import com.test.cbr.cbrtest.domain.interact.EventBusBroadcastReceiver;
import com.test.cbr.cbrtest.domain.interact.EventBusMock;
import com.test.cbr.cbrtest.domain.model.CurrencyTag;
import com.test.cbr.cbrtest.domain.model.IModel;
import com.test.cbr.cbrtest.domain.model.Model;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_PROVIDED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * model unit test
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class ModelUnitTest {

    final IModel model = new Model();

    IEventSubscriber subscriber = new IEventSubscriber() {
        @Override
        public void receiveInfoData(String what, IDataHolder dataHolder) {
            if(what.equals(CURRENCIES_PROVIDED))
            {
                assertEquals(true, model.updateCurrencies(dataHolder.getCurrencies()));
            }
        }
    };

    @Test
    public void testModelCorrectData() throws Exception {

        List<CurrencyTag> list = new ArrayList<>();
        list.add(new CurrencyTag("1", 654, "USD", 1, "name1", "32,5"));
        list.add(new CurrencyTag("2", 655, "USD", 1, "name1", "32,5"));

        IDataProvider source = new DataProviderMock(new DataHolder(list), new EventBusMock(subscriber));
        source.loadData();

        assertEquals(true, model.setInputValue(new BigDecimal(432.543)));
        assertEquals(true, model.setInputCurrency(0));
        assertEquals(true, model.setOutputCurrency(1));
        assertNotEquals(null, model.getCurrency(0));
        assertNotEquals(null, model.getCurrency(1));
        assertEquals(2, model.getCurrenciesCount());
        assertNotEquals(null, model.calculateResultValue());
    }

    @Test
    public void testModelInсorrectData1() throws Exception {

        List<CurrencyTag> list = new ArrayList<>();
        list.add(new CurrencyTag("1", 654, "USD", 1, "name1", "32,5"));
        list.add(new CurrencyTag("2", 655, "USD", 1, "name1", "32,5"));

        IDataProvider source = new DataProviderMock(new DataHolder(list), new EventBusMock(subscriber));
        source.loadData();

        assertEquals(false, model.setInputValue(null));
        assertEquals(true, model.setInputCurrency(0));
        assertEquals(true, model.setOutputCurrency(1));
        assertNotEquals(null, model.calculateResultValue());
    }

    @Test
    public void testModelnсorrectData2() throws Exception {

        List<CurrencyTag> list = new ArrayList<>();
        list.add(new CurrencyTag("1", 654, "USD", 1, "name1", "32,5"));
        list.add(new CurrencyTag("2", 655, "USD", 1, "name1", "32,5"));

        IDataProvider source = new DataProviderMock(new DataHolder(list), new EventBusMock(subscriber));
        source.loadData();

        assertEquals(true, model.setInputValue(new BigDecimal(432.543)));
        assertEquals(false, model.updateCurrencies(null));
        assertEquals(true, model.setInputCurrency(0));
        assertEquals(true, model.setOutputCurrency(1));
        assertNotEquals(null, model.calculateResultValue());
    }

    @Test
    public void testModelInсorrectData3() throws Exception {

        List<CurrencyTag> list = new ArrayList<>();
        list.add(new CurrencyTag("1", 654, "USD", 1, "name1", "32,5"));
        list.add(new CurrencyTag("2", 655, "USD", 1, "name1", "32,5"));

        IDataProvider source = new DataProviderMock(new DataHolder(list), new EventBusMock(subscriber));
        source.loadData();

        assertEquals(true, model.setInputValue(new BigDecimal(432.543)));
        assertEquals(false, model.setInputCurrency(3));
        assertEquals(true, model.setOutputCurrency(1));
        assertNotEquals(null, model.calculateResultValue());
    }

    @Test
    public void testModelInсorrectData4() throws Exception {

        List<CurrencyTag> list = new ArrayList<>();
        list.add(new CurrencyTag("1", 654, "USD", 1, "name1", "32,5"));
        list.add(new CurrencyTag("2", 655, "USD", 1, "name1", "32,5"));

        IDataProvider source = new DataProviderMock(new DataHolder(list), new EventBusMock(subscriber));
        source.loadData();

        assertEquals(true, model.setInputValue(new BigDecimal(432.543)));
        assertEquals(true, model.setInputCurrency(0));
        assertEquals(false, model.setOutputCurrency(5));
        assertNotEquals(null, model.calculateResultValue());
    }

    @Test
    public void testModelInсorrectData5() throws Exception {

        List<CurrencyTag> list = new ArrayList<>();

        IDataProvider source = new DataProviderMock(new DataHolder(list), new EventBusMock(subscriber));
        source.loadData();

        assertEquals(true, model.setInputValue(new BigDecimal(432.543)));
        assertEquals(false, model.setInputCurrency(0));
        assertEquals(false, model.setOutputCurrency(1));
        assertEquals(null, model.calculateResultValue());
    }

    @Test
    public void testModelInсorrectData6() throws Exception {

        List<CurrencyTag> list = new ArrayList<>();
        list.add(new CurrencyTag("1", 654, "USD", 1, "name1", "32,5"));
        list.add(new CurrencyTag("2", 655, "USD", 1, "name1", "32,5"));

        IDataProvider source = new DataProviderMock(new DataHolder(list), new EventBusMock(subscriber));
        source.loadData();

        assertEquals(true, model.setInputValue(new BigDecimal(432.543)));
        assertEquals(true, model.setInputCurrency(0));
        assertEquals(true, model.setOutputCurrency(1));
        assertEquals(null, model.getCurrency(3));
        assertNotEquals(null, model.getCurrency(1));
        assertEquals(2, model.getCurrenciesCount());
        assertNotEquals(null, model.calculateResultValue());
    }

    @Test
    public void testModelInсorrectData7() throws Exception {

        List<CurrencyTag> list = new ArrayList<>();
        list.add(new CurrencyTag("1", 654, "USD", 1, "name1", "32,5"));
        list.add(new CurrencyTag("2", 655, "USD", 1, "name1", "32,5"));

        IDataProvider source = new DataProviderMock(new DataHolder(list), new EventBusMock(subscriber));
        source.loadData();

        assertEquals(true, model.setInputValue(new BigDecimal(432.543)));
        assertEquals(true, model.setInputCurrency(0));
        assertEquals(true, model.setOutputCurrency(1));
        assertNotEquals(null, model.getCurrency(0));
        assertEquals(null, model.getCurrency(5));
        assertEquals(2, model.getCurrenciesCount());
        assertNotEquals(null, model.calculateResultValue());
    }

    @Test
    public void testModelInсorrectData8() throws Exception {

        List<CurrencyTag> list = new ArrayList<>();
        list.add(new CurrencyTag("1", 654, "USD", 1, "name1", "32,5"));
        list.add(new CurrencyTag("2", 655, "USD", 1, "name1", "32,5"));

        IDataProvider source = new DataProviderMock(new DataHolder(list), new EventBusMock(subscriber));
        source.loadData();

        assertEquals(true, model.setInputValue(new BigDecimal(432.543)));
        assertEquals(true, model.setInputCurrency(0));
        assertEquals(true, model.setOutputCurrency(1));
        assertNotEquals(null, model.getCurrency(0));
        assertNotEquals(null, model.getCurrency(1));
        assertNotEquals(3, model.getCurrenciesCount());
        assertNotEquals(null, model.calculateResultValue());
    }

    @Test
    public void testModelInсorrectData9() throws Exception {

        List<CurrencyTag> list = new ArrayList<>();
        list.add(new CurrencyTag(null, 654, "USD", 1, "name1", "32,5"));
        list.add(new CurrencyTag("2", 655, "USD", 1, "name1", "32,5"));

        IDataProvider source = new DataProviderMock(new DataHolder(list), new EventBusMock(subscriber));
        source.loadData();

        assertEquals(true, model.setInputValue(new BigDecimal(432.543)));
        assertEquals(true, model.setInputCurrency(0));
        assertEquals(true, model.setOutputCurrency(1));
        assertNotEquals(null, model.getCurrency(0));
        assertNotEquals(null, model.getCurrency(1));
        assertNotEquals(3, model.getCurrenciesCount());
        assertEquals(null, model.calculateResultValue());
    }
}