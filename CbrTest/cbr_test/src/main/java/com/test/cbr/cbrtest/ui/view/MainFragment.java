/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.TextView;

import com.test.cbr.cbrtest.DI.serviceprovider.IServiceProvider;
import com.test.cbr.cbrtest.R;
import com.test.cbr.cbrtest.ui.presenter.Presenter;
import com.test.cbr.cbrtest.utils.ButtonSafe;
import com.test.cbr.cbrtest.utils.IEditTextChanged;
import com.test.cbr.cbrtest.utils.ProgressBarSafe;
import com.test.cbr.cbrtest.utils.Routines;
import com.test.cbr.cbrtest.utils.SpinnerSafe;
import com.test.cbr.cbrtest.utils.TextViewSafe;

/**
 * main activity fragment
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class MainFragment extends BaseFragment implements IView {

    final private static String TAG = "MainFragment";

    private TextViewSafe editText = new TextViewSafe(this, R.id.editText);
    private TextViewSafe resultText = new TextViewSafe(this, R.id.resultText);
    private ProgressBarSafe progressBar = new ProgressBarSafe(this, R.id.loadProgress);
    private ButtonSafe calculateButton = new ButtonSafe(this, R.id.calculateButton);
    private SpinnerSafe currencyIn = new SpinnerSafe(this, R.id.currencyIn);
    private SpinnerSafe currencyOut = new SpinnerSafe(this, R.id.currencyOut);

    private Presenter presenter = new Presenter(this);
    private ICurrencyAdapter currencySpinnerAdapter;

    @Override
    public void inject(IServiceProvider serviceProvider) {
        super.inject(serviceProvider);
        presenter.inject(serviceProvider);
        currencySpinnerAdapter = serviceProvider.getCurrencyAdapter();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        editText.setTextChangedEvent( new IEditTextChanged() {
            @Override
            public void onTextChanged(TextView editText, CharSequence s) {
                presenter.setInputValue(s.toString());
            }
        });

        currencyIn.setSpinnerAdapter(currencySpinnerAdapter);
        currencyOut.setSpinnerAdapter(currencySpinnerAdapter);

        currencyIn.setSpinnerItemClick(new SpinnerSafe.OnItemSelected() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                presenter.setInputCurrency(position);
            }
        });

        currencyOut.setSpinnerItemClick(new SpinnerSafe.OnItemSelected() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                presenter.setOutputCurrency(position);
            }
        });

        calculateButton.setViewClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.calculateResultValue();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.loadCurrencies();
    }

    @Override
    public void showResultValue(String result) {
        inputMethodManagerSafe.hideSoftInputFromWindow(resultText.getWindowToken(), 0);
        resultText.setViewText(result);
        resultText.baseAnimation("alpha", 0, 1, 1000);
        resultText.baseAnimation("scaleX", 2, 1, 1000);
        resultText.baseAnimation("scaleY", 2, 1, 1000);
    }

    @Override
    public void loadCurrenciesStarted() {
        progressBar.setViewVisible(View.VISIBLE);
        calculateButton.setViewEnable(false);
    }

    @Override
    public void onCurrenciesLoadSuccess() {
        progressBar.setViewVisible(View.GONE);
        calculateButton.setViewEnable(true);

        if (currencySpinnerAdapter != null) {
            currencySpinnerAdapter.update();
        }
    }

    @Override
    public void onCurrenciesLoadFail() {
        progressBar.setViewVisible(View.GONE);
        showToast(R.string.load_failed);
    }

    @Override
    public void onCurrenciesLoadEmpty() {
        progressBar.setViewVisible(View.GONE);
        showToast(R.string.load_empty);
    }
}
