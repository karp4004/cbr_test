package com.test.cbr.cbrtest.data.dataholder;

import com.test.cbr.cbrtest.domain.model.CurrencyTag;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 26.02.2017.
 */

public class DataHolder implements IDataHolder{

    List<CurrencyTag> currencies = new ArrayList<>();

    public DataHolder()
    {
    }

    public DataHolder(List<CurrencyTag> currencies)
    {
        this.currencies = currencies;
    }

    @Override
    public List<CurrencyTag> getCurrencies() {
        return currencies;
    }

    @Override
    public void setCurrencies(List<CurrencyTag> currencies) {
        this.currencies = currencies;
    }
}
