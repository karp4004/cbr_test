/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.stream;

import android.util.Log;

import com.test.cbr.cbrtest.utils.Routines;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/**
 * data stream
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class StreamReader implements IStreamReader {

    final private static String TAG = "StreamReader";

    private String readEncoding(InputStream inputStream) throws java.io.IOException {
        int eindex = 0;
        char[] encoding = new char[]{'e', 'n', 'c', 'o', 'd', 'i', 'n', 'g', '=', '\"'};

        String encodingString = "";

        char s = '\n';

        while (true) {
            s = (char) inputStream.read();

            Log.i(TAG, "s:" + s);

            if (eindex < encoding.length) {
                if (s == encoding[eindex]) {
                    eindex++;
                } else {
                    eindex = 0;
                    if (s == encoding[eindex]) {
                        eindex++;
                    }
                }
            } else {
                if (s == '\"') {
                    break;
                }

                encodingString += s;
            }

            if (s == '\n' || s == -1) {
                break;
            }
        }

        while (true) {
            if (s == '\n' || s == -1) {
                break;
            }

            s = (char) inputStream.read();
        }

        return encodingString;
    }

    @Override
    public byte[] read(InputStream inputStream) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            Log.i(TAG, "outputStream:" + outputStream);

            while (inputStream.available() > 0) {
                byte[] buff = new byte[inputStream.available()];

                //Log.i(TAG, "buff:" + buff);

                int res = inputStream.read(buff);

                //Log.i(TAG, "res:" + res);

                if (res > -1) {
                    if (res > 0) {
                        outputStream.write(buff, 0, res);
                    }
                } else {
                    break;
                }
            }

            return outputStream.toByteArray();
        } catch (IOException e) {
            Routines.handleException(TAG, e);
        }

        return null;
    }

    public String readString(InputStream inputStream) {
        try {
            String encoding = readEncoding(inputStream);

            Log.i(TAG, "encoding:" + encoding);

            byte[] contentData = read(inputStream);

            Log.i(TAG, "contentData:" + contentData);

            if (contentData != null) {
                String encodedString = new String(contentData, encoding);

                Log.i(TAG, "encodedString:" + encodedString);

                return encodedString;
            }
        } catch (UnsupportedEncodingException e) {
            Routines.handleException(TAG, e);
        } catch (IOException e) {
            Routines.handleException(TAG, e);
        }

        return null;
    }
}
