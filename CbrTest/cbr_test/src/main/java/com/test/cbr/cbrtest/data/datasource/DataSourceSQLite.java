/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.data.datasource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.test.cbr.cbrtest.data.dataholder.DataHolder;
import com.test.cbr.cbrtest.data.dataholder.IDataHolder;
import com.test.cbr.cbrtest.domain.events.IEventBus;
import com.test.cbr.cbrtest.domain.model.CurrencyTag;
import com.test.cbr.cbrtest.utils.Routines;

import java.util.ArrayList;
import java.util.List;

import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_LOADED_LOCAL;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_LOADED_LOCAL_FAIL;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_UPDATED_LOCAL;

/**
 * SQLiter module for local currencies data
 * <p>
 * <P>Provide currencies data
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class DataSourceSQLite extends SQLiteOpenHelper implements IDataSource {

    final private static String TAG = "DataSourceSQLite";

    private IEventBus eventBus;

    public DataSourceSQLite(Context context, IEventBus eventBus) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.eventBus = eventBus;
    }

    public void onCreate(final SQLiteDatabase db) {
        try {
            if (db != null) {
                db.execSQL(SQL_CREATE_ENTRIES);
            }
        } catch (SQLException e) {
            Routines.handleException(TAG, e);
        }
    }

    public void onUpgrade(final SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            if (db != null) {
                db.execSQL(SQL_DELETE_ENTRIES);
                onCreate(db);
            }
        } catch (SQLException e) {
            Routines.handleException(TAG, e);
        }
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (db != null) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }


    private void closeDB(SQLiteDatabase db) {
        if (db != null) {
            db.close();
        }
    }

    private boolean deleteCurrency(List<CurrencyTag> currencyTagList) {

        SQLiteDatabase db = null;
        try {//TODO: try-with-resource, KITKAT
            db = getWritableDatabase();
            if (db != null) {
                for (CurrencyTag c :
                        currencyTagList) {
                    String selection = "currency_id LIKE ?";
                    String[] selectionArgs = {"" + c.getID()};
                    db.delete(TABLE_NAME, selection, selectionArgs);
                }
            }
        } catch (SQLiteException e) {
            Routines.handleException(TAG, e);
        } finally {
            closeDB(db);
        }

        return true;
    }

    private boolean saveCurrencies(List<CurrencyTag> currencyTagList) {
        SQLiteDatabase db = null;
        try {//TODO: try-with-resource, KITKAT
            db = getWritableDatabase();
            if (db != null) {
                boolean ret = true;
                for (CurrencyTag c :
                        currencyTagList) {
                    ContentValues values = new ContentValues();
                    values.put("currency_id", c.getID());
                    values.put("num_code", c.getNumCode());
                    values.put("char_code", c.getCharCode());
                    values.put("nominal", c.getNominal());
                    values.put("name", c.getName());
                    values.put("value", c.getValue());
                    long res = db.insert(TABLE_NAME, null, values);
                    if (res < 0) {
                        ret = false;
                    }
                }
                
                return ret;
            }
        } catch (SQLiteException e) {
            Routines.handleException(TAG, e);
        } finally {
            closeDB(db);
        }

        return false;
    }

    public void loadData() {
        SQLiteDatabase db = null;
        try {//TODO: try-with-resource, KITKAT
            db = getReadableDatabase();
            if (db != null) {
                String[] projection = {
                        "currency_id",
                        "num_code",
                        "char_code",
                        "nominal",
                        "name",
                        "value"
                };

                Cursor cursor = db.query(
                        TABLE_NAME,
                        projection,
                        null,
                        null,
                        null,
                        null,
                        "name ASC"
                );

                if (cursor != null) {
                    final ArrayList<CurrencyTag> currencies = new ArrayList<>();
                    while (cursor.moveToNext()) {
                        try {
                            String currency_id = cursor.getString(
                                    cursor.getColumnIndexOrThrow("currency_id"));

                            CurrencyTag currencyTag = new CurrencyTag();
                            currencyTag.setId(currency_id);
                            currencyTag.setNumCode(cursor.getInt(cursor.getColumnIndexOrThrow("num_code")));
                            currencyTag.setCharCode(cursor.getString(cursor.getColumnIndexOrThrow("char_code")));
                            currencyTag.setNominal(cursor.getInt(cursor.getColumnIndexOrThrow("nominal")));
                            currencyTag.setName(cursor.getString(cursor.getColumnIndexOrThrow("name")));
                            currencyTag.setValue(cursor.getString(cursor.getColumnIndexOrThrow("value")));

                            currencies.add(currencyTag);
                        } catch (IllegalArgumentException e) {
                            Routines.handleException(TAG, e);
                        }
                    }

                    cursor.close();

                    if(currencies.size() > 0) {
                        eventBus.publish(CURRENCIES_LOADED_LOCAL, new DataHolder(currencies));
                        return;
                    }
                }
            }
        } catch (SQLiteException e) {
            Routines.handleException(TAG, e);
        } finally {
            closeDB(db);
        }

        eventBus.publish(CURRENCIES_LOADED_LOCAL_FAIL, new DataHolder());
    }

    public void updateData(IDataHolder dataHolder) {

        SQLiteDatabase db = null;
        try {//TODO: try-with-resource, KITKAT
            db = getReadableDatabase();
            if (db != null) {
                List<CurrencyTag> newCurrencies = new ArrayList<>();
                for (CurrencyTag c :
                        dataHolder.getCurrencies()) {
                    ContentValues values = new ContentValues();
                    values.put("num_code", c.getNumCode());
                    values.put("char_code", c.getCharCode());
                    values.put("nominal", c.getNominal());
                    values.put("name", c.getName());
                    values.put("value", c.getValue());

                    String selection = "currency_id LIKE ?";
                    String[] selectionArgs = {"" + c.getID()};

                    int res = db.update(
                            TABLE_NAME,
                            values,
                            selection,
                            selectionArgs);

                    if (res < 1) {
                        newCurrencies.add(c);
                    }
                }

                if(saveCurrencies(newCurrencies))
                {
                    eventBus.publish(CURRENCIES_UPDATED_LOCAL, new DataHolder(newCurrencies));
                    return;
                }
            }
        } catch (SQLiteException e) {
            Routines.handleException(TAG, e);
        } finally {
            closeDB(db);
        }

        eventBus.publish(CURRENCIES_UPDATED_LOCAL, new DataHolder());
    }

    final private static String SQL_CREATE_ENTRIES =
            "CREATE TABLE Currencies (currency_id TEXT" +
                    ", num_code INT" +
                    ", char_code TEXT" +
                    ", nominal REAL" +
                    ", name TEXT" +
                    ", value TEXT);";

    final private static String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS Currencies;";

    final private static int DATABASE_VERSION = 1;
    final private static String DATABASE_NAME = "Currencies.db";
    final private static String TABLE_NAME = "Currencies";
}
