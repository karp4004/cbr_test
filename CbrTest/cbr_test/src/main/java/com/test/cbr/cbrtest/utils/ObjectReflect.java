/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.utils;

import android.util.Log;

import java.lang.reflect.Field;

/**
 * print field with reflectt
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class ObjectReflect extends Object {

    final private static String TAG = "ObjectReflect";

    public void print(String tag) {
        try {
            for (Field f :
                    getClass().getDeclaredFields()) {
                f.setAccessible(true);
                Log.i(tag, "" + f.getName() + ":" + f.get(this));
            }
        } catch (IllegalAccessException e) {
            Routines.handleException(TAG, e);
        } catch (IllegalArgumentException e) {
            Routines.handleException(TAG, e);
        }
    }

    public Class getSerialClass() {
        return ObjectReflect.class;
    }
}
