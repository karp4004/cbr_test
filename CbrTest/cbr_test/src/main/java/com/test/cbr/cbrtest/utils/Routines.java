/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.utils;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.Currency;

/**
 * common routines
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class Routines {

    final static String TAG = "Routines";

    public static void handleException(String tag, Exception e) {
        Log.i(tag, "e:" + e);
        e.printStackTrace();
    }

    public static void setSpinnerAdapter(Spinner spinner, SpinnerAdapter adapter) {
        try {
            spinner.setAdapter(adapter);
        } catch (NullPointerException e) {
            handleException(TAG, e);
        }
    }

    public static void setSpinnerItemClick(Spinner spinner, AdapterView.OnItemSelectedListener list) {
        try {
            spinner.setOnItemSelectedListener(list);
        } catch (NullPointerException e) {
            handleException(TAG, e);
        }
    }

    public static String stringReplase(String str, String replaced, String replace) {
        try {
            return str.replace(replaced, replace);
        } catch (NullPointerException e) {
            handleException(TAG, e);
        }

        return str;
    }

    public static String getCurrencySymbolFromCharCode(String charCode) {
        try {
            return Currency.getInstance(charCode).getSymbol();
        }
        catch(IllegalArgumentException e)
        {
            Routines.handleException(TAG, e);
            //return "";
        }
        catch (NullPointerException e) {
            Routines.handleException(TAG, e);
        }

        return charCode;
    }
}
