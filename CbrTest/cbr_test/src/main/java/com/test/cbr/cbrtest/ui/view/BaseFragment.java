package com.test.cbr.cbrtest.ui.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.test.cbr.cbrtest.DI.serviceprovider.IServiceProvider;
import com.test.cbr.cbrtest.R;
import com.test.cbr.cbrtest.utils.InputMethodManagerSafe;
import com.test.cbr.cbrtest.utils.ViewSafe;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by BIG-3 on 28.03.2017.
 */

public class BaseFragment extends Fragment {

    private static final String TAG = "BaseFragment";

    protected InputMethodManagerSafe inputMethodManagerSafe;
    private CopyOnWriteArrayList<ViewSafe> views = new CopyOnWriteArrayList<>();

    public void showToast(int toast)
    {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), getActivity().getResources().getString(toast), Toast.LENGTH_LONG).show();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.main_fragment, null);
        assingViews(view);
        return view;
    }

    public void inject(IServiceProvider serviceProvider)
    {
        inputMethodManagerSafe = serviceProvider.getInputMethodManagerSafe();
    }

    public void assingViews(View view)
    {
        for (ViewSafe v:
             views) {
            v.setView(view.findViewById(v.getViewId()));
        }
    }

    public void addView(ViewSafe viewSafe)
    {
        if(viewSafe != null)
        {
            views.add(viewSafe);
        }
    }
}
