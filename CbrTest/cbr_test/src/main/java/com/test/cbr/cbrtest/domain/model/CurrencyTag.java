/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.domain.model;

import com.test.cbr.cbrtest.utils.ObjectReflect;
import com.test.cbr.cbrtest.utils.Routines;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

/**
 * xml-bind class, currencies
 * <p>
 * <P>Provide currencies data
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

@Root(name = "Valute")
public class CurrencyTag extends ObjectReflect {

    final private static String TAG = "CurrencyTag";

    public CurrencyTag() {
    }

    public CurrencyTag(String id, int numCode, String charCode, int nominal, String name, String value) {
        this.id = id;
        this.numCode = numCode;
        this.charCode = charCode;
        this.nominal = nominal;
        this.name = name;
        this.value = value;
    }

    public void setCharCode(String charCode) {
        this.charCode = charCode;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNominal(int nominal) {
        this.nominal = nominal;
    }

    public void setNumCode(int numCode) {
        this.numCode = numCode;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getID() {
        return id;
    }

    public int getNominal() {
        return nominal;
    }

    public int getNumCode() {
        return numCode;
    }

    public String getCharCode() {
        return charCode;
    }

    public String getValue() {
        return value;
    }

    public boolean isValid() {
        if (id == null || id.length() < 1) {
            return false;
        }

        if (numCode < 0) {
            return false;
        }

        if (charCode == null || charCode.length() < 1) {
            return false;
        }

        if (nominal < 1) {
            return false;
        }

        if (name == null || name.length() < 1) {
            return false;
        }

        if (value == null || name.length() < 1) {
            return false;
        }

        try {
            DecimalFormatSymbols symbol = new DecimalFormatSymbols();
            String separator = "" + symbol.getDecimalSeparator();
            String inputValute = Routines.stringReplase(getValue(), ",", separator);
            NumberFormat.getInstance().parse(inputValute);
        } catch (Exception e) {
            Routines.handleException(TAG, e);
            return false;
        }

        return true;
    }

    @Attribute(name = "ID")
    private String id;

    @Element(name = "NumCode")
    private int numCode;

    @Element(name = "CharCode")
    private String charCode;

    @Element(name = "Nominal")
    private int nominal;

    @Element(name = "Name")
    private String name;

    @Element(name = "Value")
    private String value;
}
