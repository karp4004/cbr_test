package com.test.cbr.cbrtest.utils;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.os.IBinder;
import android.view.View;

import com.test.cbr.cbrtest.ui.view.BaseFragment;

/**
 * Created by BIG-3 on 28.03.2017.
 */

public abstract class ViewSafe {

    private static String TAG = "ViewSafe";

    protected int viewId = 0;
    protected View view;

    public ViewSafe(View parent, int viewId)
    {
        view = parent.findViewById(viewId);
    }

    public ViewSafe(BaseFragment baseFragment, int viewId)
    {
        baseFragment.addView(this);
        this.viewId = viewId;
    }

    public void setView(View view) {
        this.view = view;
    }

    public int getViewId() {
        return viewId;
    }

    public IBinder getWindowToken() {
        if(view != null) {
            return view.getWindowToken();
        }

        return null;
    }

    public void baseAnimation(final String param, final float start, final float end, final int dur) {
        try {
            final ObjectAnimator mover = ObjectAnimator.ofFloat(view,
                    param, start, end);
            mover.setDuration(dur);

            AnimatorSet set = new AnimatorSet();
            set.play(mover);
            set.start();
        } catch (NullPointerException e) {
            Routines.handleException(TAG, e);
        }
    }

    public void setViewEnable(boolean enabled) {
        try {
            view.setEnabled(enabled);
        } catch (NullPointerException e) {
            Routines.handleException(TAG, e);
        }
    }

    public void setViewVisible(int visible) {
        try {
            view.setVisibility(visible);
        } catch (NullPointerException e) {
            Routines.handleException(TAG, e);
        }
    }

    public void setViewClickListener(View.OnClickListener list) {
        try {
            view.setOnClickListener(list);
        } catch (NullPointerException e) {
            Routines.handleException(TAG, e);
        }
    }
}
