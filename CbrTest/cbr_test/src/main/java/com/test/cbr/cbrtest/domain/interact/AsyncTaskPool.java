package com.test.cbr.cbrtest.domain.interact;

import com.test.cbr.cbrtest.utils.Routines;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by BIG-3 on 09.03.2017.
 */

public class AsyncTaskPool {

    private final static String TAG = "AsyncTaskPool";

    private static final int KEEP_ALIVE_TIME = 1;
    private static final int CORE_POOL_SIZE = 8;
    private static final int MAXIMUM_POOL_SIZE = 8;
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

    private final BlockingQueue<Runnable> mWorkQueue = new LinkedBlockingQueue<Runnable>();

    public AsyncTaskPool()
    {
        try {
            threadPoolExecutor = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE,
                    KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, mWorkQueue);
        }
        catch (Exception e)
        {
            Routines.handleException(TAG, e);
        }
    }

    public void execute(Runnable runnable)
    {
        try {
            threadPoolExecutor.execute(runnable);
        }
        catch (Exception e)
        {
            Routines.handleException(TAG, e);
        }
    }

    private ThreadPoolExecutor threadPoolExecutor;
}
