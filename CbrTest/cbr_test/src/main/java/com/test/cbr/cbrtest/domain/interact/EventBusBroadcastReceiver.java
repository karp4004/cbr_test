package com.test.cbr.cbrtest.domain.interact;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.test.cbr.cbrtest.data.dataholder.DataHolder;
import com.test.cbr.cbrtest.data.dataholder.IDataHolder;
import com.test.cbr.cbrtest.domain.events.IEventBus;
import com.test.cbr.cbrtest.domain.events.IEventSubscriber;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by admin on 26.03.2017.
 */

public class EventBusBroadcastReceiver implements IEventBus {

    private Context context;
    private ConcurrentHashMap<IEventSubscriber, BroadcastReceiver> subscribers = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Intent, IDataHolder> messages = new ConcurrentHashMap<>();

    public EventBusBroadcastReceiver(Context context)
    {
        this.context = context;
    }

    @Override
    public void subscribe(String[] eventType, final IEventSubscriber subscriber) {

        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent != null) {
                    subscriber.receiveInfoData(intent.getAction(), messages.get(intent));
                }
                else
                {
                    subscriber.receiveInfoData(RECEIVE_ERROR, new DataHolder());
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        for (String event:
            eventType) {
            filter.addAction(event);
        }

        LocalBroadcastManager.getInstance(context).registerReceiver(broadcastReceiver, filter);
        subscribers.put(subscriber, broadcastReceiver);
    }

    @Override
    public void unsubscribe(String[] eventType, IEventSubscriber subscriber) {
        BroadcastReceiver broadcastReceiver = subscribers.get(subscriber);
        if(broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(broadcastReceiver);
            subscribers.remove(subscriber);
        }
    }

    @Override
    public void publish(String what, IDataHolder dataHolder) {
        if(dataHolder != null) {
            Intent intent = new Intent();
            intent.setAction(what);
            messages.put(intent, dataHolder);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }
    }

    @Override
    public void unsubscribeAll() {
        for (BroadcastReceiver receiver:
             subscribers.values()) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
        }
    }
}