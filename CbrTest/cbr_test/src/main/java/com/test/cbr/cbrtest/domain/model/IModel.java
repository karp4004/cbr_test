/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.domain.model;

import java.math.BigDecimal;
import java.util.List;

/**
 * model interface
 * <p>
 * <P>Provide currencies data
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public interface IModel {
    boolean setInputValue(BigDecimal number);

    boolean updateCurrencies(List<CurrencyTag> currencyTagList);

    CurrencyTag getCurrency(int idx);

    int getCurrenciesCount();

    boolean setInputCurrency(int idx);

    boolean setOutputCurrency(int idx);

    String calculateResultValue();
}
