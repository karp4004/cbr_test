/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.DI.serviceprovider;

import android.content.Context;

import com.test.cbr.cbrtest.data.repository.CurrencyRepository;
import com.test.cbr.cbrtest.data.repository.CurrencyRepositoryLocal;
import com.test.cbr.cbrtest.data.repository.CurrencyRepositoryRemote;
import com.test.cbr.cbrtest.data.repository.IDataProvider;
import com.test.cbr.cbrtest.data.datasource.DataSourceSQLite;
import com.test.cbr.cbrtest.domain.events.IEventBus;
import com.test.cbr.cbrtest.domain.model.IModel;
import com.test.cbr.cbrtest.domain.model.Model;
import com.test.cbr.cbrtest.httpclient.HttpClient;
import com.test.cbr.cbrtest.httpclient.HttpConnection;
import com.test.cbr.cbrtest.httpclient.IHttpClient;
import com.test.cbr.cbrtest.httpclient.IHttpConnection;
import com.test.cbr.cbrtest.domain.interact.EventBusBroadcastReceiver;
import com.test.cbr.cbrtest.parser.IXmlParser;
import com.test.cbr.cbrtest.parser.IZipParser;
import com.test.cbr.cbrtest.parser.XmlParser;
import com.test.cbr.cbrtest.parser.ZipParser;
import com.test.cbr.cbrtest.stream.IStreamReader;
import com.test.cbr.cbrtest.stream.StreamReader;
import com.test.cbr.cbrtest.ui.presenter.Presenter;
import com.test.cbr.cbrtest.ui.view.CurrencyAdapter;
import com.test.cbr.cbrtest.ui.view.ICurrencyAdapter;
import com.test.cbr.cbrtest.utils.InputMethodManagerSafe;

/**
 * DI service provider
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class ServiceProvider implements IServiceProvider {

    public ServiceProvider(Context ctx) {
        context = ctx;

        eventBus = new EventBusBroadcastReceiver(context);

        currencyProvider = new CurrencyRepository(eventBus, new CurrencyRepositoryLocal(new DataSourceSQLite(context, eventBus)),
                new CurrencyRepositoryRemote(eventBus, getXmlParser(), getHttpClient()));

        model = new Model();
    }

    @Override
    public IDataProvider getCurrencyProvider() {
        return currencyProvider;
    }

    @Override
    public IModel getModel() {
        return model;
    }

    @Override
    public IXmlParser getXmlParser() {
        return new XmlParser();
    }

    @Override
    public IHttpClient getHttpClient() {
        return new HttpClient(getHttpConnection());
    }

    @Override
    public IStreamReader getStreamReader() {
        return new StreamReader();
    }

    @Override
    public IZipParser getZipParser() {
        return new ZipParser();
    }

    @Override
    public IHttpConnection getHttpConnection() {
        return new HttpConnection(getStreamReader(), getZipParser());
    }

    @Override
    public ICurrencyAdapter getCurrencyAdapter() {
        return new CurrencyAdapter(getModel(), context);
    }

    @Override
    public InputMethodManagerSafe getInputMethodManagerSafe() {
        return new InputMethodManagerSafe(context);
    }

    @Override
    public IEventBus getEventBus() {
        return eventBus;
    }

    private IModel model;
    private IDataProvider currencyProvider;
    private Context context;
    private IEventBus eventBus;
}

