package com.test.cbr.cbrtest.domain.events;

import com.test.cbr.cbrtest.data.dataholder.IDataHolder;

/**
 * Created by BIG-3 on 09.03.2017.
 */

public interface IEventSubscriber{
    void receiveInfoData(String what, IDataHolder dataHolder);
}
