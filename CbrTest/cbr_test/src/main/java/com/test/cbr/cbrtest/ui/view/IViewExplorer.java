package com.test.cbr.cbrtest.ui.view;

import android.support.v4.app.Fragment;

/**
 * Created by BIG-3 on 28.03.2017.
 */

public interface IViewExplorer {
    <T extends BaseFragment> void viewFragment(T fragment);
}
