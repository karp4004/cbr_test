/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.httpclient;

/**
 * Http-client, gzip support
 * <p>
 * <P>Provide currencies data
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class HttpClient implements IHttpClient {

    final private static String TAG = "HttpClient";

    public HttpClient(IHttpConnection httpConnection) {
        this.httpConnection = httpConnection;
    }

    public boolean openUri(String url) {
        if (httpConnection != null) {
            return httpConnection.openConnection(url);
        }

        return false;
    }

    public boolean addHeaderField(String key, String value) {
        if (httpConnection != null) {
            return httpConnection.addHeaderField(key, value);
        }

        return false;
    }

    public String executeRequest() {
        if (httpConnection != null) {
            int code = httpConnection.executeRequest();
            if (code == 200) {
                return httpConnection.getHttpContentAsString();
            }
        }

        return null;
    }

    private IHttpConnection httpConnection;
}
