/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.data.repository;

import com.test.cbr.cbrtest.data.dataholder.DataHolder;
import com.test.cbr.cbrtest.data.dataholder.IDataHolder;
import com.test.cbr.cbrtest.domain.events.IEventBus;
import com.test.cbr.cbrtest.httpclient.IHttpClient;
import com.test.cbr.cbrtest.domain.interact.AsyncTaskPool;
import com.test.cbr.cbrtest.parser.IXmlParser;

import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_LOADED_REMOTE;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_LOADED_REMOTE_FAIL;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_UPDATED_REMOTE;

/**
 * Data provider from remote web service
 * <p>
 * <P>Provides data
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class CurrencyRepositoryRemote extends AsyncTaskPool implements IDataProvider {

    private final static String TAG = "CurrencyRepositoryWeb";
    private final static String hostUrl = "http://www.cbr.ru/scripts/XML_daily.asp";

    private IXmlParser xmlParser;
    private IHttpClient httpClient;
    private IEventBus eventBus;

    public CurrencyRepositoryRemote(IEventBus eventBus, IXmlParser xml, IHttpClient http) {
        xmlParser = xml;
        httpClient = http;
        this.eventBus = eventBus;
    }

    @Override
    public void loadData() {
        execute(new Runnable() {
            @Override
            public void run() {
                if (httpClient != null) {
                    if (httpClient.openUri(hostUrl)) {
                        if (httpClient.addHeaderField("Accept-Encoding", "gzip")) {
                            String content = httpClient.executeRequest();
                            if (content != null && content.length() > 0) {
                                IDataHolder dataHolder = xmlParser.fromXmlString(content);
                                if(dataHolder != null)
                                {
                                    eventBus.publish(CURRENCIES_LOADED_REMOTE, dataHolder);
                                    return;
                                }
                            }
                        }
                    }
                }

                eventBus.publish(CURRENCIES_LOADED_REMOTE_FAIL, new DataHolder());
            }
        });
    }

    @Override
    public void updateData(final IDataHolder dataHolder) {
        execute(new Runnable() {
            @Override
            public void run() {
                eventBus.publish(CURRENCIES_UPDATED_REMOTE, dataHolder);
            }
        });
    }
}
