/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.DI.serviceprovider;

import com.test.cbr.cbrtest.data.repository.IDataProvider;
import com.test.cbr.cbrtest.domain.events.IEventBus;
import com.test.cbr.cbrtest.domain.model.IModel;
import com.test.cbr.cbrtest.httpclient.IHttpClient;
import com.test.cbr.cbrtest.httpclient.IHttpConnection;
import com.test.cbr.cbrtest.parser.IXmlParser;
import com.test.cbr.cbrtest.parser.IZipParser;
import com.test.cbr.cbrtest.stream.IStreamReader;
import com.test.cbr.cbrtest.ui.presenter.Presenter;
import com.test.cbr.cbrtest.ui.view.ICurrencyAdapter;
import com.test.cbr.cbrtest.utils.InputMethodManagerSafe;

/**
 * DI service provider interface
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public interface IServiceProvider {

    IDataProvider getCurrencyProvider();

    IModel getModel();

    IXmlParser getXmlParser();

    IHttpClient getHttpClient();

    IStreamReader getStreamReader();

    IZipParser getZipParser();

    IHttpConnection getHttpConnection();

    ICurrencyAdapter getCurrencyAdapter();

    IEventBus getEventBus();

    InputMethodManagerSafe getInputMethodManagerSafe();
}
