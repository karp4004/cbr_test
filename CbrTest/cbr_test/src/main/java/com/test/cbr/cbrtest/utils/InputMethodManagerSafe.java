package com.test.cbr.cbrtest.utils;

import android.content.Context;
import android.os.IBinder;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by BIG-3 on 28.03.2017.
 */

public class InputMethodManagerSafe {

    InputMethodManager inputMethodManager;
    public InputMethodManagerSafe(Context context)
    {
        inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    public boolean hideSoftInputFromWindow(IBinder windowToken, int flags) {
        if(inputMethodManager != null) {
            if(windowToken != null) {
                return inputMethodManager.hideSoftInputFromWindow(windowToken, flags, null);
            }
        }

        return false;
    }
}
