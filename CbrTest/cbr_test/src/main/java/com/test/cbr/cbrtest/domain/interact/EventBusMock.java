package com.test.cbr.cbrtest.domain.interact;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.test.cbr.cbrtest.data.dataholder.IDataHolder;
import com.test.cbr.cbrtest.domain.events.IEventBus;
import com.test.cbr.cbrtest.domain.events.IEventSubscriber;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by admin on 26.03.2017.
 */

public class EventBusMock implements IEventBus {

    private IEventSubscriber subscriber;

    public EventBusMock(IEventSubscriber subscriber)
    {
        this.subscriber = subscriber;
    }

    @Override
    public void subscribe(String[] eventType, IEventSubscriber subscriber) {

    }

    @Override
    public void unsubscribe(String[] eventType, IEventSubscriber subscriber) {

    }

    @Override
    public void publish(String what, IDataHolder info) {
        subscriber.receiveInfoData(what, info);
    }

    @Override
    public void unsubscribeAll() {

    }
}