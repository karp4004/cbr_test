/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.parser;

import com.test.cbr.cbrtest.data.dataholder.DataHolder;
import com.test.cbr.cbrtest.data.dataholder.IDataHolder;
import com.test.cbr.cbrtest.domain.model.CurrencyList;
import com.test.cbr.cbrtest.utils.Routines;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

/**
 * xml parser, SimpleXML
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class XmlParser implements IXmlParser {

    final private static String TAG = "XmlParser";

    @Override
    public IDataHolder fromXmlString(String encodedString) {
        try {
            Serializer serializer = new Persister();
            CurrencyList list = serializer.read(CurrencyList.class, encodedString);
            if(list != null)
            {
                return new DataHolder(list.getCurrencies());
            }
        } catch (Exception e) {
            Routines.handleException(TAG, e);
        }

        return null;
    }
}
