package com.test.cbr.cbrtest.domain.events;

import com.test.cbr.cbrtest.data.dataholder.IDataHolder;

/**
 * Created by BIG-3 on 09.03.2017.
 */

public interface IEventBus {

    String CURRENCIES_LOADED_REMOTE = "com.test.cbr.cbrtest.CURRENCIES_LOADED_REMOTE";
    String CURRENCIES_LOADED_REMOTE_FAIL = "com.test.cbr.cbrtest.CURRENCIES_LOADED_REMOTE_FAIL";
    String CURRENCIES_UPDATED_REMOTE = "com.test.cbr.cbrtest.CURRENCIES_UPDATED_REMOTE";
    String CURRENCIES_UPDATED_REMOTE_FAIL = "com.test.cbr.cbrtest.CURRENCIES_UPDATED_REMOTE_FAIL";

    String CURRENCIES_LOADED_LOCAL = "com.test.cbr.cbrtest.CURRENCIES_LOADED_LOCAL";
    String CURRENCIES_LOADED_LOCAL_FAIL = "com.test.cbr.cbrtest.CURRENCIES_LOADED_LOCAL_FAIL";
    String CURRENCIES_UPDATED_LOCAL = "com.test.cbr.cbrtest.CURRENCIES_UPDATED_LOCAL";
    String CURRENCIES_UPDATED_LOCAL_FAIL = "com.test.cbr.cbrtest.CURRENCIES_UPDATED_LOCAL_FAIL";

    String CURRENCIES_PROVIDED = "com.test.cbr.cbrtest.CURRENCIES_PROVIDED";
    String CURRENCIES_PROVIDED_FAIL = "com.test.cbr.cbrtest.CURRENCIES_PROVIDED_FAIL";
    String RECEIVE_ERROR = "com.test.cbr.cbrtest.RECEIVE_ERROR";

    void subscribe(String[] eventType, IEventSubscriber subscriber);
    void publish(String what, IDataHolder info);
    void unsubscribe(String[] eventType, IEventSubscriber subscriber);
    void unsubscribeAll();
}
