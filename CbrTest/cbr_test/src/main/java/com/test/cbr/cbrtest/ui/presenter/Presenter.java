/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.ui.presenter;

import android.util.Log;

import com.test.cbr.cbrtest.data.dataholder.IDataHolder;
import com.test.cbr.cbrtest.data.repository.IDataProvider;
import com.test.cbr.cbrtest.domain.events.IEventBus;
import com.test.cbr.cbrtest.domain.events.IEventSubscriber;
import com.test.cbr.cbrtest.domain.model.IModel;
import com.test.cbr.cbrtest.DI.serviceprovider.IServiceProvider;
import com.test.cbr.cbrtest.ui.view.IView;
import com.test.cbr.cbrtest.utils.Routines;

import java.math.BigDecimal;

import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_PROVIDED;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_PROVIDED_FAIL;

/**
 * presenter, mvp
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class Presenter implements IEventSubscriber {

    final private static String TAG = "Presenter";

    public Presenter(IView view) {
        this.view = view;
    }

    public void inject(IServiceProvider serviceProvider)
    {
        this.eventBus = serviceProvider.getEventBus();
        this.model = serviceProvider.getModel();
        this.currencyProvider = serviceProvider.getCurrencyProvider();

        eventBus.subscribe(new String[]{
                CURRENCIES_PROVIDED,
                CURRENCIES_PROVIDED_FAIL
        }, this);
    }

    public boolean setInputValue(String value) {
        try {
            BigDecimal number = new BigDecimal(value);
            return model.setInputValue(number);
        } catch (NumberFormatException e) {
            Routines.handleException(TAG, e);
        } catch (NullPointerException e) {
            Routines.handleException(TAG, e);
        }

        return false;
    }

    public boolean loadCurrencies() {
        if (currencyProvider != null && view != null) {
            view.loadCurrenciesStarted();
            currencyProvider.loadData();
            return true;
        }

        return false;
    }

    @Override
    public void receiveInfoData(String what, IDataHolder dataHolder) {

        Log.i(TAG, "what:" + what);

        if(what.equals(CURRENCIES_PROVIDED))
        {
            if (dataHolder != null && dataHolder.getCurrencies() != null && dataHolder.getCurrencies().size() > 0) {
                model.updateCurrencies(dataHolder.getCurrencies());
                view.onCurrenciesLoadSuccess();
            } else {
                view.onCurrenciesLoadEmpty();
            }
        }
        else if(what.equals(CURRENCIES_PROVIDED_FAIL))
        {
            view.onCurrenciesLoadFail();
        }
    }

    public boolean setInputCurrency(int idx) {
        return model.setInputCurrency(idx);
    }

    public boolean setOutputCurrency(int idx) {
        return model.setOutputCurrency(idx);
    }

    public boolean calculateResultValue() {
        String ret = model.calculateResultValue();
        if (ret != null) {
            view.showResultValue(ret);
            return true;
        }

        return false;
    }

    private IModel model;
    private IEventBus eventBus;
    private IView view;
    private IDataProvider currencyProvider;

}
