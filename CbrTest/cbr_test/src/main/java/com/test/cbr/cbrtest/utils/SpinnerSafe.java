package com.test.cbr.cbrtest.utils;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.test.cbr.cbrtest.ui.view.BaseFragment;

/**
 * Created by BIG-3 on 28.03.2017.
 */

public class SpinnerSafe extends ViewSafe {

    private static String TAG = "SpinnerSafe";

    public interface OnItemSelected
    {
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id);
    }

    public SpinnerSafe(BaseFragment baseFragment, int viewId)
    {
        super(baseFragment, viewId);
    }

    public void setSpinnerAdapter(SpinnerAdapter adapter) {
        try {
            ((Spinner)view).setAdapter(adapter);
        } catch (NullPointerException e) {
            Routines.handleException(TAG, e);
        }
    }

    public void setSpinnerItemClick(final OnItemSelected list) {
        try {
            ((Spinner)view).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    list.onItemSelected(parent, view, position, id);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (NullPointerException e) {
            Routines.handleException(TAG, e);
        }
    }
}
