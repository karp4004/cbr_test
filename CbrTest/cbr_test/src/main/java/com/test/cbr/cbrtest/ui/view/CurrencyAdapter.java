/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.ui.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.test.cbr.cbrtest.R;
import com.test.cbr.cbrtest.domain.model.CurrencyTag;
import com.test.cbr.cbrtest.domain.model.IModel;
import com.test.cbr.cbrtest.utils.Routines;
import com.test.cbr.cbrtest.utils.TextViewSafe;

/**
 * currency list adapter
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class CurrencyAdapter extends BaseAdapter implements ICurrencyAdapter {

    public CurrencyAdapter(IModel m, Context ctx) {
        model = m;
        context = ctx;
    }

    @Override
    public void update() {
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.spinner_item, null);
        }

        if (model != null) {
            CurrencyTag currencyTag = model.getCurrency(position);
            if (currencyTag != null) {
                new TextViewSafe(convertView, R.id.txtView).setViewText(currencyTag.getName());
            }
        }

        return convertView;
    }

    @Override
    public int getCount() {
        if (model != null) {
            return model.getCurrenciesCount();
        }

        return 0;
    }

    private IModel model;
    private Context context;
}
