package com.test.cbr.cbrtest.data.dataholder;

import com.test.cbr.cbrtest.domain.model.CurrencyTag;

import java.util.List;

/**
 * Created by admin on 26.02.2017.
 */

public interface IDataHolder {
    List<CurrencyTag> getCurrencies();
    void setCurrencies(List<CurrencyTag> currencies);
}
