/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.ui.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.test.cbr.cbrtest.DI.serviceprovider.IServiceProvider;
import com.test.cbr.cbrtest.DI.serviceprovider.ServiceProvider;
import com.test.cbr.cbrtest.R;

/**
 * main activity
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class MainActivity extends AppCompatActivity  implements IViewExplorer{

    final private static String TAG = "MainActivity";

    protected IServiceProvider serviceProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        serviceProvider = new ServiceProvider(getApplicationContext());
        viewFragment(new MainFragment());
    }

    @Override
    public <T extends BaseFragment> void viewFragment(T fragment) {
        fragment.inject(serviceProvider);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        serviceProvider.getEventBus().unsubscribeAll();
    }
}
