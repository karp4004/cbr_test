/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.data.repository;

import android.util.Log;

import com.test.cbr.cbrtest.data.dataholder.IDataHolder;
import com.test.cbr.cbrtest.domain.events.IEventBus;
import com.test.cbr.cbrtest.domain.events.IEventSubscriber;

import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_LOADED_LOCAL;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_LOADED_LOCAL_FAIL;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_LOADED_REMOTE;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_LOADED_REMOTE_FAIL;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_PROVIDED;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_PROVIDED_FAIL;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_UPDATED_LOCAL;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_UPDATED_LOCAL_FAIL;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_UPDATED_REMOTE;
import static com.test.cbr.cbrtest.domain.events.IEventBus.CURRENCIES_UPDATED_REMOTE_FAIL;

/**
 * Data provider
 * <p>
 * <P>Provides data
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class CurrencyRepository implements IDataProvider, IEventSubscriber {

    final private static String TAG = "DataProvider";

    private IDataProvider dataProviderLocal;
    private IDataProvider dataProviderRemote;
    private IEventBus eventBus;

    public CurrencyRepository(IEventBus eventBus, IDataProvider dataProviderLocal, IDataProvider dataProviderRemote) {
        this.dataProviderLocal = dataProviderLocal;
        this.dataProviderRemote = dataProviderRemote;
        this.eventBus = eventBus;
        eventBus.subscribe(new String[]{
                CURRENCIES_LOADED_REMOTE,
                CURRENCIES_LOADED_REMOTE_FAIL,
                CURRENCIES_LOADED_LOCAL,
                CURRENCIES_LOADED_LOCAL_FAIL,
                CURRENCIES_UPDATED_REMOTE,
                CURRENCIES_UPDATED_REMOTE_FAIL,
                CURRENCIES_UPDATED_LOCAL,
                CURRENCIES_UPDATED_LOCAL_FAIL
        }, this);
    }

    @Override
    public void loadData() {
        if (dataProviderLocal != null) {
            dataProviderLocal.loadData();
        }
    }

    @Override
    public void updateData(final IDataHolder dataHolder) {
        if (dataProviderRemote != null) {
            dataProviderRemote.updateData(dataHolder);
        }
    }

    @Override
    public void receiveInfoData(String what, IDataHolder dataHolder) {

        Log.i(TAG, "what:" + what);

        if(what.equals(CURRENCIES_LOADED_REMOTE))
        {
            dataProviderLocal.updateData(dataHolder);
        }
        else if(what.equals(CURRENCIES_LOADED_REMOTE_FAIL))
        {
            eventBus.publish(CURRENCIES_PROVIDED_FAIL, dataHolder);
        }
        else if(what.equals(CURRENCIES_LOADED_LOCAL))
        {
            eventBus.publish(CURRENCIES_PROVIDED, dataHolder);
        }
        else if(what.equals(CURRENCIES_LOADED_LOCAL_FAIL))
        {
            dataProviderRemote.loadData();
        }
        else if(what.equals(CURRENCIES_UPDATED_LOCAL))
        {
            eventBus.publish(CURRENCIES_PROVIDED, dataHolder);
        }
        else if(what.equals(CURRENCIES_UPDATED_LOCAL_FAIL))
        {
            dataProviderRemote.loadData();
        }
        else if(what.equals(CURRENCIES_UPDATED_REMOTE))
        {
            eventBus.publish(CURRENCIES_PROVIDED, dataHolder);
        }
        else if(what.equals(CURRENCIES_UPDATED_REMOTE_FAIL))
        {
            dataProviderRemote.loadData();
        }
    }
}
