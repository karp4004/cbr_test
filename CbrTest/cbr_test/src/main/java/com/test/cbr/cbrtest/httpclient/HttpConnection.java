/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.httpclient;

import android.util.Log;

import com.test.cbr.cbrtest.parser.IZipParser;
import com.test.cbr.cbrtest.stream.IStreamReader;
import com.test.cbr.cbrtest.utils.Routines;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Http-client connection, gzip support
 * <p>
 * <P>Provide currencies data
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class HttpConnection implements IHttpConnection {

    final private static String TAG = "HttpConnection";

    public HttpConnection(IStreamReader streamReader, IZipParser zipParser) {
        this.streamReader = streamReader;
        this.zipParser = zipParser;
    }

    @Override
    public boolean openConnection(String urlString) {
        try {

            Log.i(TAG, "urlString:" + urlString);

            URL url = new URL(urlString);

            Log.i(TAG, "url:" + url);

            urlConnection = (HttpURLConnection) url.openConnection();

            Log.i(TAG, "urlConnection:" + urlConnection);

            return true;
        } catch (MalformedURLException e) {
            Routines.handleException(TAG, e);
        } catch (IOException e) {
            Routines.handleException(TAG, e);
        }

        return false;
    }

    public boolean addHeaderField(String key, String value) {
        if (urlConnection != null) {
            urlConnection.setRequestProperty(key, value);
            return true;
        }

        return false;
    }

    public int executeRequest() {
        try {
            if (urlConnection != null) {
                urlConnection.connect();

                int code = urlConnection.getResponseCode();
                Log.i(TAG, "code:" + code);

                return code;
            }
        } catch (IOException e) {
            Routines.handleException(TAG, e);
        }

        return -1;
    }

    @Override
    public String getHttpContentAsString() {

        try {
            if (urlConnection != null) {
                InputStream inputStream = urlConnection.getInputStream();
                String encoding = urlConnection.getHeaderField("Content-Encoding");
                boolean gzipped = (encoding != null && encoding.equalsIgnoreCase("gzip"));
                if (gzipped) {
                    if (zipParser != null) {
                        inputStream = zipParser.getDecompressedStream(inputStream);
                    } else {
                        return null;
                    }
                }

                if (inputStream != null) {
                    return streamReader.readString(inputStream);
                }
            }

        } catch (IOException e) {
            Routines.handleException(TAG, e);
        }


        return null;
    }

    private HttpURLConnection urlConnection;
    private IStreamReader streamReader;
    private IZipParser zipParser;
}
