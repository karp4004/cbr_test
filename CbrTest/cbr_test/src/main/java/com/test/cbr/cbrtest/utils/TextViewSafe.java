package com.test.cbr.cbrtest.utils;

import android.app.Activity;
import android.os.IBinder;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.test.cbr.cbrtest.ui.view.BaseFragment;

/**
 * Created by BIG-3 on 28.03.2017.
 */

public class TextViewSafe extends ViewSafe {

    private static String TAG = "TextViewSafe";

    public TextViewSafe(View parent, int viewId)
    {
        super(parent, viewId);
    }

    public TextViewSafe(BaseFragment baseFragment, int viewId)
    {
        super(baseFragment, viewId);
    }

    public void setTextChangedEvent(final IEditTextChanged watcher) {
        try {
            ((TextView)view).addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    watcher.onTextChanged((TextView)view, s);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        } catch (NullPointerException e) {
            Routines.handleException(TAG, e);
        }
    }

    public void setViewText(String text) {
        try {
            ((TextView)view).setText(text);
        } catch (NullPointerException e) {
            Routines.handleException(TAG, e);
        }
    }
}
