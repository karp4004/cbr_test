/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.domain.model;

import com.test.cbr.cbrtest.utils.Routines;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.List;

/**
 * data model
 * <p>
 * <P>Calculate currencies
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class Model implements IModel {

    final private static String TAG = "Model";

    public boolean setInputValue(BigDecimal number) {
        if (number != null) {
            inputValue = number;
            return true;
        }

        return false;
    }

    @Override
    public boolean updateCurrencies(List<CurrencyTag> currencyTagList) {
        if (currencyTagList != null) {
            currencies = currencyTagList;
            return true;
        }

        return false;
    }

    @Override
    public CurrencyTag getCurrency(int idx) {
        if (checkIndex(idx)) {
            return currencies.get(idx);
        }

        return null;
    }

    @Override
    public int getCurrenciesCount() {
        if (currencies != null) {
            return currencies.size();
        }

        return 0;
    }

    private boolean checkIndex(int idx) {
        if (currencies != null) {
            if (idx >= 0 && idx < currencies.size()) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean setInputCurrency(int idx) {
        if (checkIndex(idx)) {
            inputCurrencyIndex = idx;
            return true;
        }

        return false;
    }

    @Override
    public boolean setOutputCurrency(int idx) {
        if (checkIndex(idx)) {
            outputCurrencyIndex = idx;
            return true;
        }

        return false;
    }

    @Override
    public String calculateResultValue() {
        try {
            if (currencies != null) {
                if (checkIndex(inputCurrencyIndex) && checkIndex(outputCurrencyIndex)) {
                    CurrencyTag inputCurrencyTag = currencies.get(inputCurrencyIndex);
                    if (inputCurrencyTag != null && inputCurrencyTag.isValid()) {

                        CurrencyTag outputCurrencyTag = currencies.get(outputCurrencyIndex);
                        if (outputCurrencyTag != null && outputCurrencyTag.isValid()) {

                            String correncySymbol = Routines.getCurrencySymbolFromCharCode(outputCurrencyTag.getCharCode());
                            if (correncySymbol != null) {

                                DecimalFormatSymbols symbol = new DecimalFormatSymbols();
                                String separator = "" + symbol.getDecimalSeparator();
                                String inputValute = Routines.stringReplase(inputCurrencyTag.getValue(), ",", separator);
                                if (inputValute != null && inputValute.length() > 0) {

                                    String outputValute = Routines.stringReplase(outputCurrencyTag.getValue(), ",", separator);
                                    if (inputValute != null || inputValute.length() > 0) {

                                        Number inputNumber = NumberFormat.getInstance().parse(inputValute);
                                        Number outputNumber = NumberFormat.getInstance().parse(outputValute);

                                        float invalue = (inputNumber.floatValue() / inputCurrencyTag.getNominal());
                                        float outvalue = (outputNumber.floatValue() / outputCurrencyTag.getNominal());
                                        float coefficient = outvalue / invalue;
                                        BigDecimal result = inputValue.divide(new BigDecimal(coefficient), MathContext.DECIMAL128);

                                        DecimalFormat format = (DecimalFormat) DecimalFormat.getCurrencyInstance();
                                        symbol.setCurrencySymbol(correncySymbol);
                                        format.setDecimalFormatSymbols(symbol);
                                        return format.format(result);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Routines.handleException(TAG, e);
        }

        return null;
    }

    private BigDecimal inputValue = new BigDecimal(0.0);
    private int inputCurrencyIndex = 0;
    private int outputCurrencyIndex = 0;
    private List<CurrencyTag> currencies;

}
