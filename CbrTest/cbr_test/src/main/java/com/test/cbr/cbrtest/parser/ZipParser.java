/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.cbr.cbrtest.parser;

import com.test.cbr.cbrtest.utils.Routines;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/**
 * zip parser, GZIPInputStream
 *
 * @author Oleg Karpov
 * @version 1.0
 *          Created on 23.02.2017.
 */

public class ZipParser implements IZipParser {

    final private static String TAG = "ZipParser";

    @Override
    public InputStream getDecompressedStream(InputStream inputStream) {
        try {
            return new GZIPInputStream(inputStream);
        } catch (IOException e) {
            Routines.handleException(TAG, e);
        }

        return null;
    }
}
